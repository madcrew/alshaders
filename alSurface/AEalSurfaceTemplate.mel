global proc bumpControlNew(string $attr)
{
	setUITemplate -pst "attributeEditorTemplate";
	attrNavigationControlGrp -label "Bump mapping" -at $attr "bumpControl";
	setUITemplate -ppt;
}

global proc bumpControlReplace(string $attr)
{
	attrNavigationControlGrp -e -at $attr "bumpControl";
}

global proc alSurfaceTransmissionControls(string $n)
{
	int $checked = `getAttr ($n+".transmissionLinkToSpecular1")`;
	editorTemplate -dimControl $n "transmissionRoughness" $checked;
	editorTemplate -dimControl $n "transmissionIor" $checked;
}
global proc alSurfaceScatteringDimControls(string $n)
{
	int $checked = `getAttr ($n+".ssSpecifyCoefficients")`;
	editorTemplate -dimControl $n "ssBalance" $checked;
	editorTemplate -dimControl $n "ssTargetColor" $checked;
	$checked = !$checked;
	editorTemplate -dimControl $n "ssScattering" $checked;
	editorTemplate -dimControl $n "ssAbsorption" $checked;
}

global proc AEalSurfaceTemplate( string $n )
{
	editorTemplate -beginScrollLayout;
		AEswatchDisplay $n;
			
		editorTemplate -beginLayout "Diffuse" -collapse 0;
			editorTemplate -label "Strength" -addControl "diffuseStrength";
			editorTemplate  -label "Color" -addControl "diffuseColor";
			editorTemplate  -label "Roughness" -addControl "diffuseRoughness";
			
			editorTemplate -beginLayout "Subsurface scattering";
				editorTemplate  -label "Mix" -addControl "sssMix";
				editorTemplate  -label "Distance" -addControl "sssRadius";
				editorTemplate  -label "Color" -addControl "sssRadiusColor";
				editorTemplate -label "Density scale" -addControl "sssDensityScale" ;
			editorTemplate -endLayout;
			
			editorTemplate -beginLayout "Advanced";
				editorTemplate -label "Extra samples" -addControl "diffuseExtraSamples";
				editorTemplate -label "Enable caustics" -addControl "diffuseEnableCaustics";
			editorTemplate -endLayout;	
		editorTemplate -endLayout;
		
		editorTemplate -beginLayout "Specular 1" -collapse 0;
			editorTemplate  -label "Strength" -addControl "specular1Strength" ;
			editorTemplate -label "Color" -addControl "specular1Color";
			editorTemplate -label "Roughness" -addControl "specular1Roughness" ;
			editorTemplate  -label "IOR" -addControl "specular1Ior";
			editorTemplate -beginLayout "Advanced";
				editorTemplate -label "Roughness depth scale" -addControl "specular1RoughnessDepthScale";
				editorTemplate -label "Extra samples" -addControl "specular1ExtraSamples";
				editorTemplate -label "Normal" -addControl "specular1Normal";
			editorTemplate -endLayout;	
		editorTemplate -endLayout;
		
		editorTemplate -beginLayout "Specular 2" -collapse 1;
			editorTemplate  -label "Strength" -addControl "specular2Strength" ;
			editorTemplate -label "Color" -addControl "specular2Color";
			editorTemplate -label "Roughness" -addControl "specular2Roughness" ;
			editorTemplate  -label "IOR" -addControl "specular2Ior";
			editorTemplate -beginLayout "Advanced";
				editorTemplate -label "Roughness depth scale" -addControl "specular2RoughnessDepthScale";
				editorTemplate -label "Extra samples" -addControl "specular2ExtraSamples";
				editorTemplate -label "Normal" -addControl "specular2Normal";
			editorTemplate -endLayout;
		editorTemplate -endLayout;
			
	editorTemplate -beginLayout "Transmission";
			editorTemplate  -label "Strength" -addControl "transmissionStrength" ;
			editorTemplate -label "Color" -addControl "transmissionColor";
			editorTemplate -label "Link to spec" -addControl "transmissionLinkToSpecular1" 
																"alSurfaceTransmissionControls";
			editorTemplate -label "Roughness" -addControl "transmissionRoughness";
			editorTemplate -dimControl $n "transmissionRoughness" 1;
			editorTemplate  -label "IOR" -addControl "transmissionIor" ;
			editorTemplate -dimControl $n "transmissionIor" 1;
			
			editorTemplate -beginLayout "Scattering";
				editorTemplate -label "Strength" -addControl "ssStrength";
				editorTemplate -label "Balance" -addControl "ssBalance";
				editorTemplate -label "In-scattering" -addControl "ssInScattering";
				editorTemplate -label "Color" -addControl "ssTargetColor";
				editorTemplate -label "Density scale" -addControl "ssDensityScale";
				editorTemplate -label "Direction" -addControl "ssDirection";
				
				editorTemplate -label "Specify coefficients" -addControl "ssSpecifyCoefficients" 
																			"alSurfaceScatteringDimControls";
				editorTemplate -label "Scattering" -addControl "ssScattering";
				editorTemplate -label "Absorption" -addControl "ssAbsorption";
	
			editorTemplate -endLayout;
			
			editorTemplate -beginLayout "Advanced";
				editorTemplate -label "Roughness depth scale" -addControl "transmissionRoughnessDepthScale";
				editorTemplate -label "Extra samples" -addControl "transmissionExtraSamples";
				editorTemplate -label "Enable internal reflections" -addControl "transmissionEnableCaustics";
			editorTemplate -endLayout;
		
	editorTemplate -endLayout; // endLayout Transmission
	
	editorTemplate -beginLayout "Emission";
		editorTemplate -label "Strength" -addControl "emissionStrength";
		editorTemplate  -label "Color"-addControl "emissionColor";
	editorTemplate -endLayout; 
	
	
	editorTemplate -beginLayout "Bump";
		editorTemplate -callCustom "bumpControlNew" "bumpControlReplace" "normalCamera";
	editorTemplate -endLayout;
		
	editorTemplate -addExtraControls;	
		
	editorTemplate -endScrollLayout;
}
